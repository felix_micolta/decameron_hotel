
## Instalacion en local 

Siga los siguientes pasos para instalar el proyecto:

1. Clonar el repositorio::
git clone https://felix_micolta@bitbucket.org/felix_micolta/decameron_hotel.git
   
2. Ingresar a la carpeta e instalar las dependencias::
cd hotel
composer update

3.Revisar las configuraciones de la bd en el archivo /config/database.php y luego 
realizar las migraciones(Para base de datos vacia)::
php artisan migrate

4. Precarga de datos:
php artisan db:seed

5. Correr el servidor:
php artisan serve
