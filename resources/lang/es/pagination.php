<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 22/09/18
 * Time: 10:15 AM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'previous' => '&laquo; Anterior',
    'next'     => 'Siguiente &raquo;',
];