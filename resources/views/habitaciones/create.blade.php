@extends("layouts.app")

@section("title", "Registrar habitacion")

@section("content")
    <br />
    {!! Form::open(['route'=> 'habitaciones.store', 'method' => 'POST']) !!}

    @include('habitaciones.form')

    {!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@endsection