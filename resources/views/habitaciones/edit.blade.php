@extends("layouts.app")

@section("title", "Actualizar habitacion")

@section("content")
    <br />
    {!! Form::model($habitacion, ['route'=> ['habitaciones.update', $habitacion], 'method' =>  'PUT']) !!}

    @include('habitaciones.form')

    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@endsection