@include('common.errors  ')

<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('name', 'Hotel') !!}
            {!! Form::select('hotel', $hoteles, null, ['placeholder' => 'Seleccione un hotel',
             'class'=> 'form-control']) !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('name', 'Acomodación') !!}
            {!! Form::select('acomodacion', $acomodaciones, null, ['placeholder' => 'Seleccione una acomodación',
            'class'=> 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('name', 'Tipo de habitación') !!}
            {!! Form::select('tipo_habitacion', $tipos_habitacion, null, ['placeholder' => 'Seleccione un tipo de habitacción',
            'class'=> 'form-control']) !!}
        </div>
    </div>
</div>