@extends("layouts.app")

@section("title", "Ver Hotel")


@section('botones')
    <div class="pull-right">

        <a href="{{route('habitaciones.create')}}" class="btn btn-sm btn-success"> Registrar Habitación </a>
        {{--<a href="{{route('habitaciones.create', $hotel->id)}}" class="btn btn-sm btn-success"> Registrar Habitación </a>--}}
    </div>
@endsection

@section("message_class")info @endsection

@section("content")

    <img class="card-img-top mx-auto d-block" style="height: 300px; width: 400px; display: block; margin: 20px"
         src="/img/decameron_hotel.jpg" data-holder-rendered="true">

    <div class="text-center">
        <div class="row">
            <div class="col-md-12">
                <b>Hotel:</b> {{$hotel->nombre}}<br>
            </div>
            <div class="col-md-6">
                <b>Nit:</b> {{$hotel->nit}}<br>
                <b>Nro. Habitaciones</b> {{$hotel->numero_habitaciones}}
            </div>
            <div class="col-md-6">
                <b>Ciudad: </b> {{\Hotel\Hotel::get_ciudad($hotel->ciudad)}}<br>
                <b>Dirección:</b> {{$hotel->direccion}}

            </div>
        </div>
        {!! Form::open(['route'=> ['hoteles.destroy', $hotel->id], 'method' => 'DELETE']) !!}

        {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}

        {!! Form::close() !!}
    </div>
    <br>
    <table id="datatable" class="table table-bordered table-condensed table-responsive-md">
        <td colspan="3" class="text-center" style="background-color: #242a30 !important; color: white;
        font-size: 16px;">Habitaciones asignadas</td>
        <tr>
            <th>
                Tipo de Habitación
            </th>
            <th>
                Acomodación
            </th>
            <th>
                Acciones
            </th>
        </tr>

        @foreach($habitaciones as $habitacion)
            <tr>
                <td>
                    {{\Hotel\TipoHabitacion::get_tipo_habitacion($habitacion->tipo_habitacion)}}
                </td>
                <td>
                    {{\Hotel\Acomodacion::get_acomodacion($habitacion->acomodacion)}}
                </td>
                <td>
                    <a href="{{route('habitaciones.edit', $habitacion->id)}}"  title="Editar"
                       class="btn btn-circle btn-icon btn-primary btn-sm"><i class="fa fa-edit"></i>
                    </a>
                    <a href="{{url('eliminarhabitacion', $habitacion->id)}}"  title="Eliminar"
                       class="btn btn-circle btn-icon btn-danger btn-sm"><i class="fa fa-trash"></i>
                    </a>

                </td>
            </tr>
        @endforeach
    </table>
@endsection