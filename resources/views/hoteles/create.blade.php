@extends("layouts.app")

@section("title", "Registrar hotel")

@section("content")
    <br />
    {!! Form::open(['route'=> 'hoteles.store', 'method' => 'POST']) !!}

    @include('hoteles.form')

    {!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@endsection