@extends("layouts.app")

@section("title", "Listar Hoteles")

@section('botones')
<div class="pull-right">

    <a href="{{route('hoteles.create')}}" class="btn btn-sm btn-success"> Registrar Hotel </a>
</div>
@endsection

@section("message_class")success @endsection
@section("content")
    <div class="row">
        @foreach($hoteles as $hotel)
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm text-center" style="margin-top: 50px; ">
                    <img class="card-img-top mx-auto d-block" style="height: 150px; width: 200px; display: block; margin: 20px"
                         src="img/decameron_hotel.jpg" data-holder-rendered="true">
                    <div class="card-body">
                        <p class="card-text"> <b>Hotel: </b>{{$hotel->nombre}}</p>
                        <p><b>Ciudad: </b>{{\Hotel\Hotel::get_ciudad($hotel->ciudad)}}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <a href="{{url("/hoteles",$hotel->id)}}" class="btn btn-sm btn-success"> Ver </a>
                                <a href="{{url("/hoteles",$hotel->id)}}/edit" class="btn btn-sm btn-primary"> Editar </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection