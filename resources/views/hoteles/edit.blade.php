@extends("layouts.app")

@section("title", "Actualizar hotel")

@section("content")
    <br />
    {!! Form::model($hotel, ['route'=> ['hoteles.update', $hotel], 'method' =>  'PUT']) !!}

    @include('hoteles.form')

    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}
@endsection