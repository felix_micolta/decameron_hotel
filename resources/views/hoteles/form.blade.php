@include('common.errors  ')

<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('name', 'Nombre') !!}
            {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('name', 'Nit') !!}
            {!! Form::text('nit', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('name', 'Ciudad') !!}
            {!! Form::select('ciudad', $ciudades, null, ['placeholder' => 'Seleccione una ciudad', 'class'=> 'form-control']) !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('name', 'Numero habitaciones') !!}
            {!! Form::text('numero_habitaciones', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::label('name', 'Dirección') !!}
            {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>