
<html>
    <head>

        <title>HotelApp - @yield('title')</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>

    <body>
        <nav class="navbar navbar-dark bg-primary">
            <a href="{{route('hoteles.index')}}" class="navbar-brand">DECAMERON All Inclusive Hotels & Resorts</a>
            @yield('botones')
        </nav>
        @if (session('status'))
            <div class="alert alert-@yield('message_class')">
                {{ session('status') }}
            </div>
        @endif
        <div class="container">
            @yield('content')
        </div>
    </body>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('#data_table').DataTable();
        } );
    </scritp>
    @yield ('javascript');
</html>