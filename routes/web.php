<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/hoteles', 'HotelController');
Route::resource('/habitaciones', 'HabitacionController');
Route::get('/eliminarhabitacion/{name}','HabitacionController@eliminarHabitacion');

//Route::get('/nombre_url', function () {
//  return 'Ruta prueba';
//});

//Route::get('/url_parametro/{parametro}', function ($parametro) {
//  return 'Prueba parametro '.$parametro;
//});

//Route::get('/url_parametro/{parametro}/parametro_opcional/{opcional?}', function ($parametro, $opcional = null) {
//  return 'Prueba parametro '. $parametro . $opcional;
//});
