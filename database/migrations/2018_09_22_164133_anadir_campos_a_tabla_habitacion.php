<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnadirCamposATablaHabitacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('habitacions', function (Blueprint $table) {
            $table->unsignedInteger('tipo_habitacion');
            $table->unsignedInteger('hotel');
            $table->unsignedInteger('acomodacion');
            $table->foreign('tipo_habitacion')->references('id')->on('tipo_habitacions');
            $table->foreign('hotel')->references('id')->on('hotels');
            $table->foreign('acomodacion')->references('id')->on('acomodacions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('habitacions', function (Blueprint $table) {
            //
        });
    }
}
