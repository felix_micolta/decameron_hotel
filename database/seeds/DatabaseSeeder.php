<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AcomodacionTableSeeder::class);
        $this->call(TipoHabitacionTableSeeder::class);
        $this->call(CiudadTableSeeder::class);
    }
}
