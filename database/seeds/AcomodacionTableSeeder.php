<?php

use Hotel\Acomodacion;
use Illuminate\Database\Seeder;

class AcomodacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Acomodacion::create([
            'nombre' => 'Sencilla'
        ]);

        Acomodacion::create([
            'nombre' => 'Doble'
        ]);

        Acomodacion::create([
            'nombre' => 'Triple'
        ]);

        Acomodacion::create([
            'nombre' => 'Cuadruple'
        ]);
    }
}
