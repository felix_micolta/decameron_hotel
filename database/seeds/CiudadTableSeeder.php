<?php

use Hotel\Ciudad;
use Illuminate\Database\Seeder;

class CiudadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ciudad::create([
            'nombre' => 'Bogota'
        ]);

        Ciudad::create([
            'nombre' => 'Cali'
        ]);

        Ciudad::create([
            'nombre' => 'Medellin'
        ]);
    }
}
