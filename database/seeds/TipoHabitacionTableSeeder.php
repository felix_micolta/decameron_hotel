<?php

use Hotel\TipoHabitacion;
use Illuminate\Database\Seeder;

class TipoHabitacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoHabitacion::create([
            'nombre' => 'Estandar'
        ]);

        TipoHabitacion::create([
            'nombre' => 'Junior'
        ]);

        TipoHabitacion::create([
            'nombre' => 'Suite'
        ]);
    }
}
