<?php

namespace Hotel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreHotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max: 50|unique:hotels,nombre',
            'ciudad' => 'required',
            'numero_habitaciones' => 'required',
            'direccion' => 'required|max: 100',
            'nit' => 'required|max: 15|unique:hotels,nit'
        ];
    }
}
