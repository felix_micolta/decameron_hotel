<?php

namespace Hotel\Http\Controllers;

use Hotel\Habitacion;
use Hotel\Hotel;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class HotelApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Consulto todos los hoteles y los envio a la vista.
        $hoteles = Hotel::orderBy('id', 'asc')->get();
        return $hoteles;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validations = Validator::make($request->all(), [

            'nombre' => 'required|max: 50|unique:hotels,nombre',
            'ciudad' => 'required',
            'numero_habitaciones' => 'required',
            'direccion' => 'required|max: 100',
            'nit' => 'required|max: 15|unique:hotels,nit'
        ]);

        if ($validations->fails()){
            return $validations->errors();
        }else {
            $hotel = new Hotel();
            $hotel->nombre = $request->input('nombre');
            $hotel->ciudad = $request->input('ciudad');
            $hotel->nit = $request->input('nit');
            $hotel->numero_habitaciones = $request->input('numero_habitaciones');
            $hotel->direccion = $request->input('direccion');
            $hotel->save();
            return "Se ha guardado el registro correctamente ".$hotel;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // Retorna el hotel que desea visualizar
        $hotel = Hotel::find($id);
        $habitaciones = Habitacion::where('hotel', '=', $id)->get();
        return $hotel."<pre>".$habitaciones;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validations = Validator::make($request->all(), [
            'ciudad' => 'required',
            'numero_habitaciones' => 'required',
            'direccion' => 'required|max: 100',
            'nombre' => [
                'required',
                'max: 50',
                Rule::unique('hotels')->ignore($id),
            ],
            'nit' => [
                'required',
                Rule::unique('hotels')->ignore($id),
            ],
        ]);
        if ($validations->fails()){
            return $validations->errors();
        }else{

            // Almaceno los datos actualizados
            $hotel = Hotel::find($id);
            $hotel->fill($request->all());
            $hotel->save();
            return $hotel;

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotel = Hotel::find($id);
        $nombre = $hotel->nombre;
        $habitaciones = Habitacion::where('hotel', '=', $id)->get();
        $habitaciones->each->delete();
        $hotel->delete();
        return "Eliminado ".$nombre;
    }
}
