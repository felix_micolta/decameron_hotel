<?php

namespace Hotel\Http\Controllers;

use Hotel\Acomodacion;
use Hotel\Habitacion;
use Hotel\Hotel;
use Hotel\TipoHabitacion;
use Illuminate\Http\Request;
use Validator;

class HabitacionApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Consulto todos los hoteles y los envio a la vista.
        $habitaciones = Habitacion::orderBy('id', 'asc')->get();
        return $habitaciones;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validations = Validator::make($request->all(), [
            'hotel' => 'required',
            'acomodacion' => 'required',
            'tipo_habitacion' => 'required'
        ]);

        if ($validations->fails()){
            return $validations->errors();
        }else {

            $rhotel = $request->input('hotel');
            $racomodacion = $request->input('acomodacion');
            $rtipo_habitacion = $request->input('tipo_habitacion');
            $hotel = Hotel::find($rhotel);
            $habitaciones = Habitacion::where('hotel', '=', $rhotel)->get();

            if ($hotel->numero_habitaciones == count($habitaciones)) {
                $validator = Validator::make($request->all(), []);
                // Lanzo error habitaciones maximas definidas
                $validator->errors()->add('tipo_habitacion', 'Cantidad máxima de habitaciones 
            permitidas alcanzada.');
                return $validator->errors();
            }


            // Consulto si existen registradas acomodaciones y tipos de habitacion en el mismo hotel
            $habitaciones_repetidas = Habitacion::where('hotel', $rhotel)->where('acomodacion', $racomodacion)
                ->where('tipo_habitacion', $rtipo_habitacion)->first();

            if (isset($habitaciones_repetidas) && $habitaciones_repetidas != null) {
                // Lanzo error de habitacion repetida
                $validator = Validator::make($request->all(), []);
                $validator->errors()->add('tipo_habitacion', 'La habitación que intentas 
                registrar ya existe');

                return $validator->errors();
            }


            // Declaro las variables necesarias
            $estandar = TipoHabitacion::where('nombre', '=', 'Estandar')->firstOrFail();
            $junior = TipoHabitacion::where('nombre', '=', 'Junior')->firstOrFail();
            $suite = TipoHabitacion::where('nombre', '=', 'Suite')->firstOrFail();

            $sencilla = Acomodacion::where('nombre', '=', 'Sencilla')->firstOrFail();
            $doble = Acomodacion::where('nombre', '=', 'Doble')->firstOrFail();
            $triple = Acomodacion::where('nombre', '=', 'Triple')->firstOrFail();
            $cuadruple = Acomodacion::where('nombre', '=', 'Cuadruple')->firstOrFail();


            $acomodaciones_estandar = array($sencilla->id, $doble->id);
            $acomodaciones_junior = array($triple->id, $cuadruple->id);
            $acomodaciones_suite = array($sencilla->id, $doble, $triple->id);

            if ($rtipo_habitacion == $estandar->id) {
                if (!in_array($racomodacion, $acomodaciones_estandar)) {
                    // Lanzo error acomodacion no permitida
                    $validator = Validator::make($request->all(), []);
                    $validator->errors()->add('acomodacion', 'La acomodacion seleccionada no esta disponible 
                     para el tipo de habitación ' . $estandar->nombre);

                    return $validator->errors();
                }
            }

            if ($rtipo_habitacion == $junior->id) {
                if (!in_array($racomodacion, $acomodaciones_junior)) {
                    // Lanzo error acomodacion no permitida
                    $validator = Validator::make($request->all(), []);
                    $validator->errors()->add('acomodacion', 'La acomodacion seleccionada no esta disponible 
                     para el tipo de habitación ' . $junior->nombre);

                    return $validator->errors();
                }
            }

            if ($rtipo_habitacion == $suite->id) {
                if (!in_array($racomodacion, $acomodaciones_suite)) {
                    // Lanzo error acomodacion no permitida
                    $validator = Validator::make($request->all(), []);
                    $validator->errors()->add('acomodacion', 'La acomodacion seleccionada no esta disponible
                     para el tipo de habitación ' . $suite->nombre);

                    return $validator->errors();
                }
            }


            //  Recibo los parametros del request y los almaceno en un nuevo objeto de Habitacion
            $habitacion = new Habitacion();
            $habitacion->hotel = $request->input('hotel');
            $habitacion->acomodacion = $request->input('acomodacion');
            $habitacion->tipo_habitacion = $request->input('tipo_habitacion');
            $habitacion->save();
            return "Guardada correctamente la habitacion para el hotel ". $hotel->nombre;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Retorna el hotel que desea visualizar
        $habitacion = Habitacion::find($id);
        return $habitacion;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validations = Validator::make($request->all(), [
            'hotel' => 'required',
            'acomodacion' => 'required',
            'tipo_habitacion' => 'required'
        ]);

        if ($validations->fails()){
            return $validations->errors();
        }else {

            // Validaciones
            // 1.Numero Maximo de habitaciones
            // 2.No se repiten acomodaciones y tipos de habitacion por hotel
            // 3.Estandar A. Sencilla - Doble
            // 4.Junior A. Triple - Cuadruple
            // 5.Suite A. Sencilla - Doble - Triple

            // Primero traemos todas las habitaciones del hotel

            $rhotel = $request->input('hotel');
            $racomodacion = $request->input('acomodacion');
            $rtipo_habitacion = $request->input('tipo_habitacion');
            $hotel = Hotel::find($rhotel);
            $habitaciones = Habitacion::where('hotel', '=', $rhotel)->get();

            if ($hotel->numero_habitaciones == count($habitaciones) - 1) {
                // Lanzo error habitaciones maximas definidas
                return redirect()->route('hoteles.show', $rhotel)->with('status', 'Cantidad máxima de habitaciones 
                permitidas alcanzada.');
            }


            // Consulto si existen registradas acomodaciones y tipos de habitacion en el mismo hotel excluyendo la actual
            $habitaciones_repetidas = Habitacion::whereNotIn('id', [$id])->where('hotel', $rhotel)->where('acomodacion', $racomodacion)
                ->where('tipo_habitacion', $rtipo_habitacion)->first();

            if (isset($habitaciones_repetidas) && $habitaciones_repetidas != null) {
                // Lanzo error de habitacion repetida
                $validator = Validator::make($request->all(), []);
                $validator->errors()->add('tipo_habitacion', 'La habitación que intentas 
                registrar ya existe');

                return $validator->errors();
            }


            // Declaro las variables necesarias
            $estandar = TipoHabitacion::where('nombre', '=', 'Estandar')->firstOrFail();
            $junior = TipoHabitacion::where('nombre', '=', 'Junior')->firstOrFail();
            $suite = TipoHabitacion::where('nombre', '=', 'Suite')->firstOrFail();

            $sencilla = Acomodacion::where('nombre', '=', 'Sencilla')->firstOrFail();
            $doble = Acomodacion::where('nombre', '=', 'Doble')->firstOrFail();
            $triple = Acomodacion::where('nombre', '=', 'Triple')->firstOrFail();
            $cuadruple = Acomodacion::where('nombre', '=', 'Cuadruple')->firstOrFail();


            $acomodaciones_estandar = array($sencilla->id, $doble->id);
            $acomodaciones_junior = array($triple->id, $cuadruple->id);
            $acomodaciones_suite = array($sencilla->id, $doble, $triple->id);

            if ($rtipo_habitacion == $estandar->id) {
                if (!in_array($racomodacion, $acomodaciones_estandar)) {
                    // Lanzo error acomodacion no permitida
                    $validator = Validator::make($request->all(), []);
                    $validator->errors()->add('acomodacion', 'La acomodacion seleccionada no esta disponible 
                     para el tipo de habitación ' . $estandar->nombre);

                    return $validator->errors();
                }
            }

            if ($rtipo_habitacion == $junior->id) {
                if (!in_array($racomodacion, $acomodaciones_junior)) {
                    // Lanzo error acomodacion no permitida
                    $validator = Validator::make($request->all(), []);
                    $validator->errors()->add('acomodacion', 'La acomodacion seleccionada no esta disponible 
                     para el tipo de habitación ' . $junior->nombre);

                    return $validator->errors();
                }
            }

            if ($rtipo_habitacion == $suite->id) {
                if (!in_array($racomodacion, $acomodaciones_suite)) {
                    // Lanzo error acomodacion no permitida
                    $validator = Validator::make($request->all(), []);
                    $validator->errors()->add('acomodacion', 'La acomodacion seleccionada no esta disponible
                     para el tipo de habitación ' . $suite->nombre);

                    return $validator->errors();
                }
            }

            // Almaceno los datos actualizados
            $habitacion = Habitacion::find($id);
            $habitacion->fill($request->all());
            $habitacion->save();

            return "Actualizada correctamente la habitacion para el hotel " . $hotel->nombre;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $habitacion = Habitacion::find($id);
        $hotel = $habitacion->hotel;
        $hotel = Hotel::find($hotel);
        $habitacion->delete();
        return "Eliminada habitacion del hotel ".$hotel->nombre;
    }
}
