<?php

namespace Hotel\Http\Controllers;

use Hotel\Ciudad;
use Hotel\Habitacion;
use Hotel\Hotel;
use Hotel\Http\Requests\UpdateHotelRequest;
use Illuminate\Http\Request;
use Hotel\Http\Requests\StoreHotelRequest;
use Illuminate\Validation\Rule;
use Validator;


class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Consulto todos los hoteles y los envio a la vista.
        $hoteles = Hotel::orderBy('id', 'asc')->get();
        return view("hoteles.index", compact('hoteles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ciudades = Ciudad::orderBy('id')->get(['id', 'nombre'])->pluck('nombre', 'id');
        return view("hoteles.create", compact('ciudades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHotelRequest $request)
    {
        //  Recibo los parametros del request y los almaceno en un nuevo objeto de Hotel
        $hotel = new Hotel();
        $hotel->nombre = $request->input('nombre');
        $hotel->ciudad = $request->input('ciudad');
        $hotel->nit = $request->input('nit');
        $hotel->numero_habitaciones = $request->input('numero_habitaciones');
        $hotel->direccion = $request->input('direccion');
        $hotel->save();

        return redirect()->route('hoteles.index')->with('status', 'Hotel registrado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Retorna el hotel que desea visualizar
        $hotel = Hotel::find($id);
        $habitaciones = Habitacion::where('hotel', '=', $id)->get();
        return view("hoteles.show", compact('hotel', 'habitaciones'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // Direcciona a la vista de edicion
        $hotel = Hotel::find($id);

        $ciudades = Ciudad::orderBy('id')->get(['id', 'nombre'])->pluck('nombre', 'id');

        return view("hoteles.edit", compact('hotel', 'ciudades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'ciudad' => 'required',
            'numero_habitaciones' => 'required',
            'direccion' => 'required|max: 100',
            'nombre' => [
                'required',
                'max: 50',
                Rule::unique('hotels')->ignore($id),
            ],
            'nit' => [
                'required',
                Rule::unique('hotels')->ignore($id),
            ],
        ])->validate();

        // Almaceno los datos actualizados
        $hotel = Hotel::find($id);
        $hotel->fill($request->all());
        $hotel->save();

        return redirect()->route('hoteles.show', [$id])->with('status', 'Hotel actualizado correctamente.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotel = Hotel::find($id);
        $habitaciones = Habitacion::where('hotel', '=', $id)->get();
        $habitaciones->each->delete();
        $hotel->delete();

        return redirect()->route('hoteles.index')->with('status', 'Hotel y habitaciones eliminados correctamente.');
    }
}
