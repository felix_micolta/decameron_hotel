<?php

namespace Hotel;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected  $fillable = ['nombre', 'ciudad', 'numero_habitaciones', 'direccion', 'nit'];

    static  function get_ciudad($id)
    {
        $ciudad = Ciudad::where('id', '=', $id)->firstOrFail();
        return $ciudad->nombre;
    }

}
