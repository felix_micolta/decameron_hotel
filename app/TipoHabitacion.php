<?php

namespace Hotel;

use Illuminate\Database\Eloquent\Model;

class TipoHabitacion extends Model
{


    static  function get_tipo_habitacion($id)
    {
        $tipo_habitacion = TipoHabitacion::where('id', '=', $id)->firstOrFail();
        return $tipo_habitacion->nombre;
    }
}
