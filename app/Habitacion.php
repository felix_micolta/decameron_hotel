<?php

namespace Hotel;

use Illuminate\Database\Eloquent\Model;

class Habitacion extends Model
{

    protected  $fillable = ['hotel', 'tipo_habitacion', 'acomodacion'];
}
