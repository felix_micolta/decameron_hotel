<?php

namespace Hotel;

use Illuminate\Database\Eloquent\Model;

class Acomodacion extends Model
{
    static  function get_acomodacion($id)
    {
        $acomodacion = Acomodacion::where('id', '=', $id)->firstOrFail();
        return $acomodacion->nombre;
    }
}
